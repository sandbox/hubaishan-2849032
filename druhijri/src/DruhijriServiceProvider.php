<?php
/**
 * @file
 * Contains Drupal\druhijri\DruHijriServiceProvider
 */

namespace Drupal\druhijri;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Modifies the language manager service.
 */
class DruhijriServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Overrides language_manager class to test domain language negotiation.
    $definition = $container->getDefinition('date.formatter');
    $definition->setClass('Drupal\druhijri\HijriDateFormatter');
  }
}
